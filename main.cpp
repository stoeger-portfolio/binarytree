#include "binary-tree.h"

int main(void)
{
	BinrayTree bt;

	bt.Insert(10);
	bt.Insert(6);
	bt.Insert(14);
	bt.Insert(5);
	bt.Insert(8);
	bt.Insert(11);
	bt.Insert(18);

	bt.PrintTreeFromMin();
	bt.PrintTreeFromMax();

	Node* node = bt.Search(10);

	bt.DestroyTree();
}