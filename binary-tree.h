#ifndef BINARY_TREE_H_
#define BINARY_TREE_H_

#include <stdio.h>

struct Node
{
	int value;
	Node* left;
	Node* right;
};

class BinrayTree 
{
public:
	explicit BinrayTree()
		: m_root(nullptr)
	{
	}

	~BinrayTree()
	{
		DestroyTree(m_root);
	}

	inline void Insert(int value)
	{
		if (m_root != nullptr)
		{
			Insert(value, m_root);
		}
		else {
			m_root = new Node();
			m_root->left = nullptr;
			m_root->right = nullptr;
			m_root->value = value;
		}
	}

	inline void DestroyTree()
	{
		DestroyTree(m_root);
	}

	inline void PrintTreeFromMin()
	{
		PrintTreeFromMin(m_root);
		printf("\n");
	}

	inline void PrintTreeFromMax()
	{
		PrintTreeFromMax(m_root);
		printf("\n");
	}

	Node* Search(int key)
	{
		if (m_root != nullptr) 
		{
			return Search(key, m_root);
		}
		return nullptr;
	}
	
private:
	inline void DestroyTree(Node* leaf)
	{
		if (leaf != nullptr)
		{
			DestroyTree(leaf->left);
			DestroyTree(leaf->right);
			delete leaf;
		}
	}

	void Insert(int key, Node* leaf);
	void PrintTreeFromMin(Node* leaf);
	void PrintTreeFromMax(Node* leaf);
	Node* Search(int key, Node* leaf);

	Node* m_root;
};

void BinrayTree::Insert(int key, Node* leaf)
{
	if (key < leaf->value)
	{
		if (leaf->left != nullptr)
		{
			Insert(key, leaf->left);
		}
		else {
			leaf->left = new Node();
			leaf->left->left = nullptr;
			leaf->left->right = nullptr;
			leaf->left->value = key;
		}
	}
	else if (key > leaf->value) {
		if (leaf->right != nullptr)
		{
			Insert(key, leaf->right);
		} else {
			leaf->right = new Node();
			leaf->right->left = nullptr;
			leaf->right->right = nullptr;
			leaf->right->value = key;
		}
	}
}

void BinrayTree::PrintTreeFromMin(Node* leaf)
{
	if (leaf != nullptr)
	{
		PrintTreeFromMin(leaf->left);
		printf("%i ", leaf->value);
		PrintTreeFromMin(leaf->right);
	}
}

void BinrayTree::PrintTreeFromMax(Node* leaf)
{
	if (leaf != nullptr)
	{
		PrintTreeFromMax(leaf->right);
		printf("%i ", leaf->value);
		PrintTreeFromMax(leaf->left);
	}
}

Node* BinrayTree::Search(int key, Node* leaf)
{
	if (leaf != nullptr)
	{
		if (key == leaf->value)
		{
			return leaf;
		}
		else if (key > leaf->value)
		{
			return Search(key, leaf->right);
		}
		else {
			return Search(key, leaf->left);
		}
	}
}

#endif BINARY_TREE_H_